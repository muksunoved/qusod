#-------------------------------------------------
#
# Project created by QtCreator 2014-10-14T15:19:43
#
#-------------------------------------------------

QT     += core gui
QT     += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qusod
TEMPLATE = app

include(src/3rdparty/QsLog/QsLog.pri)

SOURCES += src/main.cpp\
        src/usodwidget.cpp \
    src/modbusmaster.cpp \
    src/3rdparty/libmodbus/modbus.c \
    src/3rdparty/libmodbus/modbus-data.c \
    src/3rdparty/libmodbus/modbus-rtu.c \
    src/3rdparty/libmodbus/modbus-tcp.c \
    src/parameters/modbusparameter.cpp \
    src/pagewidget.cpp \
    src/usodevice.cpp \
    src/setidpasswdialog.cpp \
    src/rawdatamodel.cpp \
    src/flashrddialog.cpp

HEADERS  += src/usodwidget.h \
    src/modbusmaster.h \
    src/3rdparty/libmodbus/modbus.h \
    src/3rdparty/libmodbus/config.h \
    src/3rdparty/libmodbus/modbus-rtu.h \
    src/3rdparty/libmodbus/modbus-tcp.h \
    src/3rdparty/libmodbus/modbus-version.h \
    src/parameters/modbusparameter.h \
    src/pagewidget.h \
    src/usodevice.h \
    src/setidpasswdialog.h \
    src/rawdatamodel.h \
    src/flashrddialog.h

FORMS    += src/forms/usodwidget.ui \
    src/forms/form.ui \
    src/forms/setidpasswdialog.ui \
    src/forms/flashrddialog.ui

OTHER_FILES +=

INCLUDEPATH += src/3rdparty/libmodbus \
    src/3rdparty/QsLog

unix:SOURCES +=

unix:DEFINES += _TTY_POSIX_

win32:SOURCES +=

win32:DEFINES += _TTY_WIN_  WINVER=0x0501 Q_OS_WIN32

win32:LIBS += -lsetupapi -lwsock32 -lws2_32

DEFINES += QS_LOG_LINE_NUMBERS     # automatically writes the file and line for each log message
#DEFINES += QS_LOG_DISABLE         # logging code is replaced with a no-op
#DEFINES += QS_LOG_SEPARATE_THREAD # messages are queued and written from a separate thread
#DEFINES += LIB_MODBUS_DEBUG_OUTPUT # enable debug output from libmodbus
