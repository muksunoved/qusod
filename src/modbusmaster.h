/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	modbusmaster.h
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/15/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */




#ifndef MODBUSMASTER_H
#define MODBUSMASTER_H

#include <stdint-gcc.h>

#include <QThread>
#include <QString>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QByteArray>
#include <QSemaphore>
#include <QQueue>

class MContainer{
public:
    MContainer(){ waiter = false;}


    quint16 *data;    
    volatile int len;
    volatile int slave;
    volatile int address;
    volatile int result;
    volatile bool          waiter;
};


class ModbusMaster : public QThread
{
    Q_OBJECT

public:

    explicit ModbusMaster(QObject *parent = 0);
    ~ModbusMaster();
    bool portconnect(  const QString& aportName, const quint32 abaud );
    void portdisconnect();
    bool isPortConnect() { return devStart; }

    static QList<QSerialPortInfo> 	availablePorts()    { return QSerialPortInfo::availablePorts();     }
    static QList<qint32>            standardBaudRates() { return QSerialPortInfo::standardBaudRates();  }

    bool write ( MContainer* w );
    bool writew( MContainer* w );
    bool read  ( MContainer* r );
    bool readw ( MContainer* r );

    bool getReading( int packetID, QByteArray& data, int len );
    bool waitReadResult( int packetID, quint32& result );

    void busMonitorRequestData(uint8_t * data, uint8_t dataLen);
    void busMonitorResponseData(uint8_t * data, uint8_t dataLen);


signals:
    void resultWr( int slave, int address );
    void resultRd( int slave, int address );
    void resulRdError( int count );
    void resulWrError( int count );

public slots:


private:
    QString portName;
    qint32 baud;

    volatile bool devFail;
    volatile bool devStart;
    volatile bool devStopped;

    QQueue<MContainer*> wrData;
    QQueue<MContainer*> rdData;        

    bool startWr;
    bool startRd;
    int rdErrors;
    int wrErrors;

    QSemaphore lockSleep;
    QSemaphore lockQWr;
    QSemaphore lockQRd;

protected:
    void run();

};

#endif // MODBUSMASTER_H

/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file modbusmaster.h
 */
