/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	usodwidget.cpp
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/14/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */




#include "usodwidget.h"
#include "ui_usodwidget.h"
#include <QSerialPortInfo>
#include "pagewidget.h"
#include "modbusmaster.h"
#include "usodevice.h"
#include <QsLog.h>
#include <QtGlobal>

UsodWidget::UsodWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UsodWidget)
{
    ui->setupUi(this);
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();

    for(int i=0; i< ports.size(); i++){
        ui->cbxPort->addItem( ports[i].portName() );
    }

    QList<qint32> bauds = QSerialPortInfo::standardBaudRates();
    for(int i=0; i< bauds.size(); i++){
        ui->cbxBaud->addItem( QString("%1").arg(bauds[i]) );
        if( bauds[i] == 9600){
            ui->cbxBaud->setCurrentIndex( ui->cbxBaud->count()-1 );
        }
    }

    ui->pbConnect->setText("Connect");
    ui->pbFind->setEnabled(false);
    ui->leFindAddress->setEnabled(false);

}


UsodWidget::~UsodWidget()
{
    delete ui;
}



void UsodWidget::on_pbConnect_released()
{
    ui->pbConnect->setEnabled(false);
    if( m.isPortConnect() ){
        m.portdisconnect();
        ui->pbConnect->setText("Connect");
        ui->cbxBaud->setEnabled(true);
        ui->cbxPort->setEnabled(true);
        ui->pbFind->setEnabled(false);
        ui->leFindAddress->setEnabled(false);
        for( int i=0; i<devPages.size(); i++){
            devPages[i]->setDisabled();
        }

    }else{


#if defined(Q_OS_WIN32)
        QString port =  ui->cbxPort->currentText() ;
#else
        QString port =  "/dev/" + ui->cbxPort->currentText() ;
#endif

        QLOG_DEBUG() << port;
        int baud = ui->cbxBaud->currentText().toInt();

        if( m.portconnect(port, baud) ) {

            ui->cbxBaud->setEnabled(false);
            ui->cbxPort->setEnabled(false);
            ui->pbFind->setEnabled(true);
            ui->pbConnect->setText("Disconnect");
            ui->leFindAddress->setEnabled(true);
            for( int i=0; i<devPages.size(); i++){
                devPages[i]->setEnabled();
            }
        }
    }
    ui->pbConnect->setEnabled(true);
}



void UsodWidget::on_pbFind_released()
{
    bool match;
    QStringList known = ui->leFindAddress->text().split(",", QString::SkipEmptyParts);
    //known.removeDuplicates();
    if( known.isEmpty() ){

    }else{
        QLOG_DEBUG() << "Checks " << known;
        for( int i=0; i<known.size(); i++){
            int address = known[i].toInt();
            if( address ){
                match = false;
                for( int j=0; j<devPages.size(); j++){
                    if( address == devPages[j]->getUsoSlaveAddress()){
                        match = true;
                        break;
                    }
                }

                if( /*!match*/1 ){ // TODO: This only for debug, restore for production!!!!!!

                    QLOG_DEBUG() << "Check at " << address;
                    USODevice* uso = USODevice::findFirstDevice(0, address, &m);
                    if( uso != 0 ){
                        uso->connectSignals();
                        PageWidget *m_page = new PageWidget(0, uso);
                        ui->tabDevices->addTab(m_page, "Dev-"+QString("%1").arg(address));
                        m_page->setEnabled();
                        connect(m_page,SIGNAL(enablePause(bool)), this,SLOT(enablePause(bool)));
                        devPages.append(m_page);
                    }
                }

            }
        }
    }
}


void UsodWidget::on_tabDevices_tabCloseRequested(int index)
{    
    QLOG_DEBUG() << "Remove tab " << index;
    devPages[index]->setDisabled();
    ui->tabDevices->removeTab( index );
    QLOG_DEBUG() << "Delete page " << index;
    devPages[index]->deleteLater();
    QLOG_DEBUG() << "Remove page " << index;
    devPages.removeAt( index );
}

void UsodWidget::enablePause(bool set)
{
    if( set ){
        for( int i=0; i<devPages.size(); i++ ){
            devPages[i]->setDisabled();
        }
    }else{
        for( int i=0; i<devPages.size(); i++ ){
            devPages[i]->setEnabled();
        }
    }
}

/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file usodwidget.cpp
 */

