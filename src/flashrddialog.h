/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	flashrddialog.h
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	5/8/2015
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2015 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */



#ifndef FLASHRDDIALOG_H
#define FLASHRDDIALOG_H

#include <QDialog>
#include <QByteArray>
#include "usodevice.h"

namespace Ui {
class FlashRdDialog;
}


class FlashReader : public QThread
{
    Q_OBJECT

public:
    explicit FlashReader( QObject *parent );
    FlashReader( QObject *parent, USODevice* ausodev, QByteArray *adata );
    void startRead();
    void stopRead();

signals:
    void currentFlashPage( int page );
    void currentSensDataTime( int seconds, int ms, bool isValid );
    void endProcess( bool result );


private:
    USODevice* usodev;
    QByteArray *data;
    bool bStart;

protected:
    void run();

};

class FlashRdDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FlashRdDialog(QWidget *parent = 0);
    FlashRdDialog( QWidget *parent, USODevice* ausodev , QByteArray *adata);
    ~FlashRdDialog();

private slots:
    void on_pbStop_released();
    void currentFlashPage( int page );
    void currentSensDataTime( int seconds, int ms, bool isValid );
    void endProcess(bool result);

    void on_FlashRdDialog_finished(int result);

private:
    Ui::FlashRdDialog *ui;
    FlashReader *reader;
};

#endif // FLASHRDDIALOG_H

/*
 * Copyright 2015 GK Grant Ufa (www.grant-ufa.ru)
 * End of file flashrddialog.h
 */


