/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	usodevice.h
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/24/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */




#ifndef USODEVICE_H
#define USODEVICE_H

#include <QObject>
#include <QByteArray>

#include "parameters/modbusparameter.h"
#include "modbusmaster.h"
#include <QDateTime>
#include <QMap>


#define USOD_PACKETS_PER_PAGE    (8)
#define USOD_FLASH_PAGE_COUNT (8192)



class USODevice : public QObject
{
    Q_OBJECT

public:    

    enum{
        platfRegs         = 0x0004/2,
        rTime             = 0x0030/2,
        sTime             = 0x003e/2,
        platfRom          = 0x0048/2,
        mbUsRom           = 0x0050/2,

        syncTimeScheduler = 0x0078/2,

        sensRegs_0        = 0x025c/2,
        sensUpdFlags      = 0x024c/2,

        flsRegs           = 0x16b8/2,
        pgToRd            = 0x16f4/2,
        typeToRd          = 0x16f8/2,
        rdBuf             = 0x16fc/2,

        cmdSetTime        = 0x1afc/2,
        cmdSetRom         = 0x1b0e/2,
        cmdSetNvRom       = 0x1b10/2,
        cmdPgRead         = 0x1b1c/2,
        cmdSendSyncro     = 0x1b46/2,
        cmdFixUpdaterData = 0x1b48/2
    };

    USODevice(QObject *parent = 0, int aslave = 0, ModbusMaster* am = 0);

    void setSlaveAddress( const int aslave ) { slave = aslave; }
    int  getSlaveAddress() { return slave; }
    bool refreshDeviceTime();
    bool refreshStaticParameters();
    bool refreshDeviceFlashOffset();
    bool refreshSensorData( int addr );
    bool refreshSensorFlags();

    void getDeviceTime( QVariant &atime );
    void getDeviceSerial( QVariant &serial );
    void getDeviceSerialRom( QVariant &serialROM );
    void getDeviceMBusSettings( QVariant &settings );
    void getDeviceFlashInfo( QVariant &info );
    void getSensorData( QVariant &d );
    void getSyncSchedule( int &sch );

    void getSensorFlags( QVariant &d );

    bool setDeviceSerial( MParameters::SSerialROM *serial );
    bool setDeviceTime( MParameters::TimeStruct *time );
    bool setDeviceMBusSettings( MParameters::MBusSettings *mb );
    bool setDeviceSyncSchedule( int val );
    bool setDeviceFlfsId( int val );
    bool setDeviceFlfsPgRd( int val );
    bool readFlfsPageTail(int tail, QByteArray &vals );
    bool setCommand( int cmd );
    bool waitCommand(int cmd, int repeat = 100 );

    void connectSignals();

    static USODevice* findFirstDevice(QObject *parent = 0, const int startDeviceAddress = 0, ModbusMaster *m = 0 );

signals:
    void updateParameterRd_SIGN( int address);
    void updateParameterWr_SIGN( int address);
    void updDeviceTime( );
    void updDeviceFlash( );
    void updSensorData();
    void updSensorUpdFlags();

public slots:
    void updateParameterRd_SLOT(int aslave, int aaddress );
    void updateParameterWr_SLOT(int aslave, int aaddress );

private:
    int slave;
    ModbusParameter uspTime;
    ModbusParameter uspTimeSet;
    ModbusParameter uspSerial;
    ModbusParameter uspSerialROM;
    ModbusParameter uspMBusData;
    ModbusParameter *uspFlFsInfo;
    ModbusParameter uspFlFsInfoS;
    ModbusParameter uspFlFsRead;
    ModbusParameter *uspFlFsPage[USOD_PACKETS_PER_PAGE];
    ModbusParameter uspCommand;
    ModbusParameter uspSensorData;
    ModbusParameter uspSensorFlags;
    ModbusParameter uspSyncTimeSched;
    ModbusParameter uspFlFsTypeToRd;

    ModbusMaster *service;
    QMap <int, ModbusParameter*> pmap;

};


#endif // USODEVICE_H

/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file usodevice.h
 */

