/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	flashrddialog.cpp
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	5/8/2015
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2015 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */



#include <QsLog.h>
#include <QFileDialog>

#include "flashrddialog.h"
#include "ui_flashrddialog.h"


FlashRdDialog::FlashRdDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FlashRdDialog)
{
    ui->setupUi(this);
}

FlashRdDialog::FlashRdDialog(QWidget *parent, USODevice *ausodev, QByteArray *adata) :
    QDialog(parent),
    ui(new Ui::FlashRdDialog)
{
    ui->setupUi(this);
    reader = new FlashReader(this, ausodev, adata );

    QLOG_DEBUG() << "Connect reader";

    connect( reader, SIGNAL(endProcess(bool)), this, SLOT(endProcess(bool)) );
    connect( reader, SIGNAL(currentFlashPage(int)), this, SLOT(currentFlashPage(int)) );
    connect( reader, SIGNAL(currentSensDataTime(int,int,bool)), this, SLOT(currentSensDataTime(int,int,bool)) );

    QLOG_DEBUG() << "Start reader";

    reader->startRead();

}

FlashRdDialog::~FlashRdDialog()
{
    delete ui;
}


void FlashRdDialog::on_pbStop_released()
{
    if(reader != NULL ){
        reader->stopRead();
        QLOG_DEBUG() << "Stop flash read";
    }
}

void FlashRdDialog::currentFlashPage(int page)
{
    ///////////////////
    ui->leCurPage->setText( QString("%1").arg((page*1024)/sizeof(SFLFSRec)) );
}

void FlashRdDialog::currentSensDataTime(int seconds, int ms, bool isValid)
{
    ////////////////////////
    if( isValid ){
        QDateTime tme = QDateTime::fromTime_t( seconds + 946684800 ).toUTC();
        ui->leDateTime->setText( tme.toString("dd-MM-yyyy hh:mm:ss") + QString(".%1").arg(ms) );
    }else{
        ui->leDateTime->setText( QString("Unknown or invalid record") );
    }
}

void FlashRdDialog::endProcess(bool result)
{
    close();
}


FlashReader::FlashReader(QObject *parent) :
    QThread( parent )
{
    bStart = false;
    usodev = NULL;
    data = NULL;
}

FlashReader::FlashReader(QObject *parent, USODevice *ausodev, QByteArray *adata):
    QThread( parent), usodev(ausodev), data(adata)
{
    bStart = false;
}

void FlashReader::startRead()
{
    if( bStart == false && usodev != NULL && data != NULL ){
        bStart = true;
        start();
    }else{
        QLOG_DEBUG() << "Error start reader " << bStart << " : " << usodev << " : " << data;
    }
}

void FlashReader::stopRead()
{
    bStart = false;
}

void FlashReader::run()
{
    int page;
    int stopPage;
    int errCount;
    bool ret = true;

    if( usodev == NULL || data == NULL ){
        ret = false;
        bStart = false;
    }else{
        QVariant tmp;
        usodev->getDeviceFlashInfo( tmp );
        stopPage = page = tmp.value<MParameters::FlashInfo>().pgWr;
        stopPage++;
        if( stopPage == USOD_FLASH_PAGE_COUNT ){
            stopPage = 0;
        }
        emit currentFlashPage( page );
        data->clear();
    }

    for(int i=page; i!=stopPage && bStart; i=((i==0)?(USOD_FLASH_PAGE_COUNT-1):(i-1)) ){

        emit currentFlashPage( i );

        usodev->setDeviceFlfsId(int(2));
        usodev->setDeviceFlfsPgRd(i);
        usodev->setCommand( (int)USODevice::cmdPgRead );
        msleep(100);
        if(usodev->waitCommand( (int)USODevice::cmdPgRead ) != true){
            QLOG_DEBUG() << "Error wait flash";
            ret = bStart = false;
            break;
        }

        QByteArray valsAccum;

        errCount = 0;

        for( int j=0; j<USOD_PACKETS_PER_PAGE; j++){
            QByteArray vals;

            if(usodev->readFlfsPageTail(j, vals) == true){
                valsAccum += vals;
                errCount = 0;
            }else{
                QLOG_DEBUG() << "Fail read page tail";
                if( ++errCount > 10 ){
                    break;
                }
            }
        }
        if( errCount ){
            bStart = false;
            ret = false;
            break;
        }

        // serialize
        SFLFSRec rec;
        char chData[sizeof(SFLFSRec)];
        for( int l=valsAccum.size()-sizeof(SFLFSRec); l>0; l-=sizeof(SFLFSRec)  ){
            for(int k=0; k<sizeof(SFLFSRec); k++){
                chData[k] = valsAccum[l+k];
            }
            memcpy((void*)&rec, (void*)chData, sizeof(SFLFSRec));
            if( rec.hdr != REC_HDR_MTU ){
                QLOG_DEBUG() << "Unknown record";
                emit currentSensDataTime( rec.con.fLongVal.seconds, rec.con.fLongVal.msec, false );
            }else{
                QLOG_DEBUG() << "page " << i<< " seconds " << rec.con.fLongVal.seconds << " addr " <<
                                rec.con.fLongVal.sensAddr << " count " << rec.con.fLongVal.counter;
                emit currentSensDataTime( rec.con.fLongVal.seconds, rec.con.fLongVal.msec, true );
            }
        }
        (*data) += valsAccum;

    }
    emit endProcess( ret );
}





/*
 * Copyright 2015 GK Grant Ufa (www.grant-ufa.ru)
 * End of file flashrddialog.cpp
 */


void FlashRdDialog::on_FlashRdDialog_finished(int result)
{
    reader->stopRead();
}
