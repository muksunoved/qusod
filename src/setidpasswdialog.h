/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	setidpasswdialog.h
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/28/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */


/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file setidpasswdialog.h
 */


#ifndef SETIDPASSWDIALOG_H
#define SETIDPASSWDIALOG_H

#include <QDialog>

namespace Ui {
class SetIDPasswDialog;
}

class SetIDPasswDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetIDPasswDialog(QWidget *parent = 0);
    ~SetIDPasswDialog();
    QString getPassString();

private:
    Ui::SetIDPasswDialog *ui;
};

#endif // SETIDPASSWDIALOG_H
