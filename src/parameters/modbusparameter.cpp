/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	modbusparameter.cpp
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/21/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */


/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file modbusparameter.cpp
 */


#include <QDateTime>
#include <QsLog.h>
#include "modbusparameter.h"



unsigned int ModbusParameter::idTimeInfo          = qRegisterMetaType<MParameters::TimeInfo>("MParameters::TimeInfo");
unsigned int ModbusParameter::idTimeStruct        = qRegisterMetaType<MParameters::TimeStruct>("MParameters::TimeStruct");
unsigned int ModbusParameter::idFlashInfo         = qRegisterMetaType<MParameters::FlashInfo>("MParameters::FlashInfo");
unsigned int ModbusParameter::idFlashInfoS        = qRegisterMetaType<MParameters::FlashInfos>("MParameters::FlashInfos");
unsigned int ModbusParameter::idFlashRead         = qRegisterMetaType<MParameters::FlashRead>("MParameters::FlashRead");
unsigned int ModbusParameter::idArray             = qRegisterMetaType<MParameters::Array>("MParameters::Array");
unsigned int ModbusParameter::idRegister          = qRegisterMetaType<MParameters::Register>("MParameters::Register");
unsigned int ModbusParameter::idRegisterPair      = qRegisterMetaType<MParameters::RegisterPair>("MParameters::RegisterPair");
unsigned int ModbusParameter::idSSerial           = qRegisterMetaType<MParameters::SSerial>("MParameters::SSerial");
unsigned int ModbusParameter::idSSerialROM        = qRegisterMetaType<MParameters::SSerialROM>("MParameters::SSerialROM");
unsigned int ModbusParameter::idMBusSettigs       = qRegisterMetaType<MParameters::MBusSettings>("MParameters::MBusSettings");
unsigned int ModbusParameter::idSensorRegs        = qRegisterMetaType<MParameters::SensorRegs>("MParameters::SensorRegs");
unsigned int ModbusParameter::idSensorUpdFlags    = qRegisterMetaType<MParameters::SensorUpdFlags>("MParameters::SensorUpdFlags");


ModbusParameter::ModbusParameter( )
{
    mContainer.address = 0;
    type = QVariant::Invalid;
    size = 0;
    valid = false;
}


ModbusParameter::ModbusParameter(int maddress, unsigned  int atype, int asize)
{


    type = atype;

    if( type == idTimeInfo ) {

        size = sizeof(MParameters::TimeInfo);

    }else if( type == idTimeStruct ) {

        size = sizeof(MParameters::TimeStruct);

    }else if( type == idFlashInfo ) {

        size = sizeof(MParameters::FlashInfo);

    }else if( type == idFlashInfoS ) {

        size = sizeof(MParameters::FlashInfos);

    }else if( type == idFlashRead ) {

        size = sizeof(MParameters::FlashRead);

    }else if( type == idArray ) {

        size = sizeof(MParameters::Array);

    }else if( type == idRegister ) {

        size = sizeof(MParameters::Register);

    }else if( type == idRegisterPair ) {

        size = sizeof(MParameters::RegisterPair);

    }else if( type == idSSerial){
        size =  sizeof(MParameters::SSerial);

    }else if( type == idSSerialROM){
        size =  sizeof(MParameters::SSerialROM);

    }else if( type == idMBusSettigs){
        size =  sizeof(MParameters::MBusSettings);

    }else if( type == idSensorRegs){
        size =  sizeof(MParameters::SensorRegs);
    }else if( type == idSensorUpdFlags){
        size =  sizeof(MParameters::SensorUpdFlags);
    }
    else{
        Q_ASSERT( asize );
        size = asize;
    }
    stor = QByteArray(size, (char)0);
    mContainer.address = maddress;
    mContainer.data = (quint16*)stor.data();
    mContainer.len = size/2;
    valid = true;
}



bool ModbusParameter::getValue(QVariant& val )
{

    Q_ASSERT( size != 0                 );
    //Q_ASSERT( type != QVariant::Invalid );

    bool result = true;

    if(val.type() == QVariant::ByteArray){

        QByteArray tmp;
        getValue( tmp );
        val.setValue( tmp );
        result = true;

    }else if(val.type() == QVariant::String){
        QString tmp;
        getValue( tmp );
        val.setValue( tmp );
        result = true;
    }
    else{

        if( type == idTimeInfo ) {

            //result.setValue( *(reinterpret_cast<MParameters::TimeInfo*>(stor.data()) ) );
            MParameters::TimeInfo *value = reinterpret_cast<MParameters::TimeInfo*>(stor.data()) ;
            QDateTime tmp = QDateTime::fromTime_t( value->seconds+946684800 ).toUTC();
            val.setValue(tmp);

        }else if( type == idTimeStruct ) {

            val.setValue( *(reinterpret_cast<MParameters::TimeStruct*>(stor.data()) ) );

        }else if( type == idFlashInfoS ) {

            val.setValue( *(reinterpret_cast<MParameters::FlashInfos*>(stor.data()) ) );

        }else if( type == idFlashInfo ) {

            val.setValue( *(reinterpret_cast<MParameters::FlashInfo*>(stor.data()) ) );

        }else if( type == idFlashRead ) {

            val.setValue( *(reinterpret_cast<MParameters::FlashRead*>(stor.data()) ) );

        }else if( type == idArray ) {

            val.setValue( *(reinterpret_cast<MParameters::Array*>(stor.data()) ) );

        }else if( type == idRegister ) {

            if( val.userType() == idRegister){
                val.setValue( *(reinterpret_cast<MParameters::Register*>(stor.data()) ) );

            }else if(val.type() == QVariant::Int){
                int tmp = (stor[0]&0xff) | ((stor[1]<<8)&0xff00);
                val.setValue( tmp );

            }else{
                result = false;
            }

        }else if( type == idRegisterPair ) {

            if( val.userType() == idRegisterPair ){
                val.setValue( *(reinterpret_cast<MParameters::RegisterPair*>(stor.data()) ) );
            }else if(val.type() == QVariant::Int){
                int tmp = (stor[0]&0xff) | ((stor[1]<<8)&0xff00) | ((stor[2]<<16)&0xff0000) |  ((stor[3]<<24)&0xff000000);
                val.setValue( tmp );
            }

        }else if( type == idSSerial ) {

            val.setValue( *(reinterpret_cast<MParameters::SSerial*>(stor.data()) ) );

        }else  if( type == idSSerialROM ) {

            val.setValue( *(reinterpret_cast<MParameters::SSerialROM*>(stor.data()) ) );

        }else  if( type == idMBusSettigs ) {

            val.setValue( *(reinterpret_cast<MParameters::MBusSettings*>(stor.data()) ) );

        }else  if( type == idSensorRegs ) {

            val.setValue( *(reinterpret_cast<MParameters::SensorRegs*>(stor.data()) ) );

        }else  if( type == idSensorUpdFlags ) {

            val.setValue( *(reinterpret_cast<MParameters::SensorUpdFlags*>(stor.data()) ) );

        }else{

            result = false;
        }
    }

    return result;

}

bool ModbusParameter::getValue(QString &val)
{
    Q_ASSERT( size != 0                 );
    Q_ASSERT( type != QVariant::Invalid );

    bool result = true;

    if( type == idTimeInfo ) {

        MParameters::TimeInfo *value = reinterpret_cast<MParameters::TimeInfo*>(stor.data()) ;
        QDateTime tmp = QDateTime::fromTime_t( value->seconds+946684800 ).toUTC();
        val = tmp.toString("dd-MM-yyyy hh:mm:ss");

    }else if( type == idRegister ) {

        MParameters::Register *value = (reinterpret_cast<MParameters::Register*>(stor.data()) );
        val = QString("%1").arg((unsigned int)value->data);

    }else if( type == idRegisterPair ) {

        MParameters::RegisterPair *value = (reinterpret_cast<MParameters::RegisterPair*>(stor.data()) );
        val = QString("%1").arg(value->data);

    }else if( type == idArray ) {
        val = stor.toHex();
    }
    else{
        val = QString(stor);
        result = false;
    }

    return result;
}

bool ModbusParameter::getValue(QByteArray &val)
{
    val = stor;
    return true;
}

bool ModbusParameter::setValue(const QVariant &avalue)
{
    bool result = true;

    if( avalue.type() == QVariant::UserType ){
        /*if( avalue.userType() == type ){
            QByteArray tmp;

            return true;
        }*/
    }else if( avalue.type() == QVariant::ByteArray){

         setValue(avalue.value<QByteArray>());

    }else if( avalue.type() == QVariant::String ){
        result = setValue( avalue.value<QString>() );

    }else if( avalue.type() == QVariant::Int ){

        if( type == ModbusParameter::idRegister ){

            QByteArray tmp;
            tmp.append( avalue.value<int>() & 0xff);
            tmp.append( (avalue.value<int>()>>8) & 0xff);
            return setValue( tmp );

        }
        if( type == ModbusParameter::idRegisterPair ){
            QByteArray tmp;

            tmp.append( avalue.value<int>() & 0xff);
            tmp.append( (avalue.value<int>()>>8) & 0xff);
            tmp.append( (avalue.value<int>()>>16) & 0xff);
            tmp.append( (avalue.value<int>()>>24) & 0xff);

            return setValue( tmp );
        }

    }
    else{
        result = false;
    }

    return result;
}

bool ModbusParameter::setValue(const QByteArray &avalue)
{
    if( size == avalue.size() ){
        for( int i=0; i<size; i++){
            stor[i] = avalue[i];
        }
        QLOG_DEBUG() << "Set value" << stor.toHex();
        return true;
    }
    return false;
}



bool ModbusParameter::setValue(const QString &str)
{
    Q_ASSERT( size != 0                 );
    Q_ASSERT( type != QVariant::Invalid );

    bool result;

    if( str.isEmpty() ){
        return false;
    }
    if( type == idRegister ) {

        if( size < 2 ){
            return false;
        }

        quint16 value = static_cast<quint16>(str.toUInt(&result));
        if( result ){
            stor[0] = value & 0x00ff;
            stor[1] = (value>>8) & 0x00ff;
        }

    }else if( type == idRegisterPair ) {

        if( size < 4 ){
            return false;
        }

        quint32 value = static_cast<quint32>(str.toUInt(&result));
        if( result ){
            stor[0] = value & 0x000000ff;
            stor[1] = (value>>8) & 0x000000ff;
            stor[2] = (value>>16) & 0x000000ff;
            stor[3] = (value>>24) & 0x000000ff;
        }

    }else{
        result = false;
    }

    return result;

}
