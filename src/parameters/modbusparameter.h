/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	modbusparameter.h
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/21/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */




#ifndef MODBUSPARAMETER_H
#define MODBUSPARAMETER_H


#include <QVariant>
#include <QMetaType>
#include "src/modbusmaster.h"


#define FLFS_MAX_INFOS   3
#define FLFS_FLASH_TYPID 2

typedef union{
  float fVal;
  int   iVal;
  char  cVal[4];
  short sVal[2];
} U_VAL;

namespace MParameters {

    struct SSerialROM{
        unsigned short password;
        unsigned short serial;
        unsigned int   enableCount;
    };

    struct SSerial{
        quint16	number;		//
        quint8	system_code;	//
        quint8	version;		//
        char	build_date[20];		//
        char	build_time[20];		//
    };

    struct TimeInfo{

        quint16 smallSec;
        quint32  seconds;
        quint16  msDef;		//1000 or 1024 time ms
        quint16  devSec;
        quint16  devMsec;
    };
    struct TimeStruct{
        quint8 bSec;
        quint8 bMin;
        quint8 bHour;
        quint8 bDate;
        quint8 bMonth;
        quint8 bWeekDay;
        quint8 bYear;
        quint8 reserved;
    };
    struct FlashInfo{
        quint16 typeId;
        quint16 blockSz;
        quint32 totalBlocks;
        quint32 pgWr; //need write to rom from any write func
        quint32 pgWrOffs;
        quint32 loops;
    };
    struct FlashInfos{
        struct FlashInfo info[FLFS_MAX_INFOS];
    };

    struct FlashRead{
        quint32  pgToRd;
        quint32  typeToRd;
    };
    struct Array{
        quint16  data[64];
    };
    struct Register{
        quint16 data;
    };
    struct RegisterPair{
        quint32 data;
    };
    struct MBusSettings{
        quint16  init;
        quint16  ptpModem;
        qint32   baud;
        quint8   address;
        quint8   reserved;
        quint16  mode;
    };

    struct SensorRegs{
        U_VAL   		cur_val;
        uint32_t	  	counter;
        uint16_t	  	id_sensor;
        uint16_t	  	address;
        uint16_t	  	timems;
        uint32_t	   	time;
        union{
            char        	vals[32];
            uint32_t        uivals[4];
        } ;
    };

    struct SensorUpdFlags{
        uint32_t flags[4];
    };
}


typedef struct{
    unsigned int 	seconds;
    unsigned short 	msec;
    unsigned short 	sensAddr;
    unsigned char	vals[16];
    unsigned int 	counter;
}__attribute__((__packed__)) SFLFSRecMTURData;


typedef union{
  unsigned char  	data[31];
  SFLFSRecMTURData 	fLongVal;
}__attribute__((__packed__)) UFLFSRecContens;


typedef struct{
   unsigned char hdr;
   UFLFSRecContens con;
} __attribute__((__packed__)) SFLFSRec;

#define REC_HDR_MTU			(0x0F)


Q_DECLARE_METATYPE(MParameters::TimeInfo)
Q_DECLARE_METATYPE(MParameters::TimeStruct)
Q_DECLARE_METATYPE(MParameters::FlashInfo)
Q_DECLARE_METATYPE(MParameters::FlashInfos)
Q_DECLARE_METATYPE(MParameters::FlashRead)
Q_DECLARE_METATYPE(MParameters::Array)
Q_DECLARE_METATYPE(MParameters::Register)
Q_DECLARE_METATYPE(MParameters::RegisterPair)
Q_DECLARE_METATYPE(MParameters::SSerial)
Q_DECLARE_METATYPE(MParameters::SSerialROM)
Q_DECLARE_METATYPE(MParameters::MBusSettings)
Q_DECLARE_METATYPE(MParameters::SensorRegs)
Q_DECLARE_METATYPE(MParameters::SensorUpdFlags)


class ModbusParameter
{

public:
    explicit ModbusParameter();
    ModbusParameter(int amadress,
                    unsigned int atype = QVariant::Invalid,
                    int asize = 0);

    int  getAddress() { return mContainer.address; }
    void setAddress( int maddress) { mContainer.address = maddress; }

    bool isValid() { return valid; }



    bool getValue(QVariant &val);
    bool getValue(QString  &val);
    bool getValue(QByteArray  &val);

    bool setValue(const QVariant &avalue );
    bool setValue(const QByteArray &avalue );
    bool setValue(const QString &str );

    MContainer* getModbusContainer() { return &mContainer; }

    static unsigned int idTimeInfo;
    static unsigned int idTimeStruct;
    static unsigned int idFlashInfo;
    static unsigned int idFlashInfoS;
    static unsigned int idFlashRead;
    static unsigned int idArray;
    static unsigned int idRegister;
    static unsigned int idRegisterPair;
    static unsigned int idSSerial;
    static unsigned int idSSerialROM;
    static unsigned int idMBusSettigs;
    static unsigned int idSensorRegs;
    static unsigned int idSensorUpdFlags;    

private:    
    MContainer mContainer;   
    QByteArray stor;
    int size;
    unsigned  int type;
    bool valid;

};

#endif // MODBUSPARAMETER_H

/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file modbusparameter.h
 */
