/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	usodevice.cpp
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/24/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */


#include "usodevice.h"
#include <QsLog.h>
#include <QByteArray>




USODevice::USODevice( QObject *parent, int aslave, ModbusMaster *am):
    QObject(parent),
    slave(aslave),
    service(am),
    uspTime(        (int)USODevice::rTime,          (int)ModbusParameter::idTimeInfo    ),
    uspTimeSet(     (int)USODevice::sTime,          (int)ModbusParameter::idTimeStruct  ),
    uspSerial(      (int)USODevice::platfRegs,      (int)ModbusParameter::idSSerial     ),
    uspSerialROM(   (int)USODevice::platfRom,       (int)ModbusParameter::idSSerialROM  ),
    uspMBusData(    (int)USODevice::mbUsRom,        (int)ModbusParameter::idMBusSettigs ),
    uspFlFsInfoS(   (int)USODevice::flsRegs,        (int)ModbusParameter::idFlashInfoS  ),
    uspFlFsRead(    (int)USODevice::pgToRd,         (int)ModbusParameter::/*idFlashRead*/idRegisterPair   ),
    uspSensorData(  (int)USODevice::sensRegs_0,     (int)ModbusParameter::idSensorRegs  ),
    uspSensorFlags( (int)USODevice::sensUpdFlags,   (int)ModbusParameter::idSensorUpdFlags  ),    
    uspFlFsInfo(0),
    uspCommand(     0x0000,   (int)ModbusParameter::idRegister ),
    uspSyncTimeSched( (int)USODevice::syncTimeScheduler, (int)ModbusParameter::idRegisterPair),
    uspFlFsTypeToRd( int(USODevice::typeToRd), int(ModbusParameter::idRegisterPair) )

{
    pmap[uspTime.getAddress()] = &uspTime;
    pmap[uspSerial.getAddress()] = &uspSerial;
    pmap[uspSerialROM.getAddress()] = &uspSerialROM;
    pmap[uspMBusData.getAddress()] = &uspMBusData;
    pmap[uspFlFsInfoS.getAddress()] = &uspFlFsInfoS;
    pmap[uspFlFsRead.getAddress()] = &uspFlFsRead;
    pmap[uspSensorData.getAddress()] = &uspSensorData;
    pmap[uspSensorFlags.getAddress()] = &uspSensorFlags;
    pmap[uspSyncTimeSched.getAddress()] = &uspSyncTimeSched;
    for( int i=0; i<USOD_PACKETS_PER_PAGE; i++){
        uspFlFsPage[i] =new ModbusParameter( (int)USODevice::rdBuf+i*sizeof(MParameters::Array)/2 , (int)ModbusParameter::idArray );
        pmap[uspFlFsPage[i]->getAddress()] = uspFlFsPage[i];
    }

}


bool USODevice::refreshDeviceTime( )
{
    MContainer* req = uspTime.getModbusContainer();
    req->slave = slave;
    return service->read( req );
}

bool USODevice::refreshStaticParameters()
{
    bool result;

    MContainer* req = uspSerial.getModbusContainer();
    req->slave = slave;
    if( (result = service->readw( req )) != true){
        QLOG_DEBUG() << "Fail get uspSerial" << req->result;
        return result;
    }

    req = uspSerialROM.getModbusContainer();
    req->slave = slave;
    if( (result = service->readw( req )) != true){
        QLOG_DEBUG() << "Fail get uspSerialRom" << req->result;
        return result;
    }

    req = uspMBusData.getModbusContainer();
    req->slave = slave;
    if( (result = service->readw( req )) != true){
        QLOG_DEBUG() << "Fail get uspMBusData" << req->result;
        return result;
    }

    req = uspSyncTimeSched.getModbusContainer();
    req->slave = slave;
    if( (result = service->readw( req )) != true){
        QLOG_DEBUG() << "Fail get uspSyncTimeSched" << req->result;
        return result;
    }


    req = uspFlFsInfoS.getModbusContainer();
    req->slave = slave;
    if( (result = service->readw( req )) != true){
        QLOG_DEBUG() << "Fail get uspFlFsInfo" << req->result;
        return result;

    }else{
        QVariant tmp;
        uspFlFsInfoS.getValue( tmp );
        for( int i=0; i<FLFS_MAX_INFOS; i++){

            if(tmp.value<MParameters::FlashInfos>().info[i].typeId == FLFS_FLASH_TYPID ){

                uspFlFsInfo = new ModbusParameter( uspFlFsInfoS.getAddress()+i*sizeof(MParameters::FlashInfo)/2, (int)ModbusParameter::idFlashInfo);
                pmap[uspFlFsInfo->getAddress()] = uspFlFsInfo;

                QLOG_DEBUG() << "Flash info --> at " << QString("0x%1").arg(uspFlFsInfo->getAddress(),0,16) << " { " <<
                                 tmp.value<MParameters::FlashInfos>().info[i].typeId <<":" <<
                                 tmp.value<MParameters::FlashInfos>().info[i].totalBlocks <<":" <<
                                 tmp.value<MParameters::FlashInfos>().info[i].pgWr<<":" <<
                                 tmp.value<MParameters::FlashInfos>().info[i].pgWrOffs << " }";
            }
        }

    }

    return true;
}

bool USODevice::refreshDeviceFlashOffset()
{
    if( uspFlFsInfo && uspFlFsInfo->isValid() ){
        MContainer *req = uspFlFsInfo->getModbusContainer();
        req->slave = slave;
        return service->read( req );
    }else{
        return false;
    }
}

bool USODevice::refreshSensorData(int addr )
{
    MContainer *req = uspSensorData.getModbusContainer();
    req->slave = slave;
    req->address = addr;
    return service->read( req );
}

bool USODevice::refreshSensorFlags()
{
    MContainer *req = uspSensorFlags.getModbusContainer();
    req->slave = slave;
    return service->read( req );
}

void USODevice::getDeviceTime(QVariant& atime)
{
    uspTime.getValue( atime );
}

void USODevice::getDeviceSerial(QVariant &serial)
{
    uspSerial.getValue( serial);
}

void USODevice::getDeviceSerialRom(QVariant &serialROM)
{
    uspSerialROM.getValue( serialROM );
}

void USODevice::getDeviceMBusSettings(QVariant &settings)
{
    uspMBusData.getValue( settings );
}

void USODevice::getDeviceFlashInfo(QVariant &info)
{
    uspFlFsInfo->getValue( info );
}

void USODevice::getSensorData(QVariant &d)
{
    uspSensorData.getValue(d);
}

void USODevice::getSyncSchedule(int &sch)
{
    QVariant val( int(0));
    uspSyncTimeSched.getValue( val );
    sch = val.value<int>();
}

void USODevice::getSensorFlags(QVariant &d)
{
    uspSensorFlags.getValue(d);
}

bool USODevice::setDeviceSerial(MParameters::SSerialROM *serial)
{
    QByteArray tmp;
    char* c = (char*)serial;
    for( int i=0; i<sizeof(MParameters::SSerialROM); i++){
        tmp.append(*c);
        c++;
    }
    if(uspSerialROM.setValue( tmp )){
        QLOG_DEBUG() << "Copy true";
        MContainer *con = uspSerialROM.getModbusContainer();
        con->slave = slave;
        return (service->write( con ));
    }else{
        QLOG_DEBUG() << "Copy false";
    }
}

bool USODevice::setDeviceTime(MParameters::TimeStruct* time)
{
    QByteArray tmp;
    char* c = (char*)time;

    for( int i=0; i<sizeof(MParameters::TimeStruct); i++){

        tmp.append(*c);
        c++;
    }

    if(uspTimeSet.setValue( tmp )){

        QLOG_DEBUG() << "Copy true";
        MContainer *con = uspTimeSet.getModbusContainer();
        con->slave = slave;
        return (service->write( con ));
    }else{
        QLOG_DEBUG() << "Copy false";
    }

}

bool USODevice::setDeviceMBusSettings(MParameters::MBusSettings *mb)
{
    QByteArray tmp;
    char* c = (char*)mb;

    for( int i=0; i<sizeof(MParameters::MBusSettings); i++){

        tmp.append(*c);
        c++;
    }

    if(uspMBusData.setValue( tmp )){

        QLOG_DEBUG() << "Copy true";
        MContainer *con = uspMBusData.getModbusContainer();
        con->slave = slave;
        return (service->write( con ));
    }else{
        QLOG_DEBUG() << "Copy false";
    }

}

bool USODevice::setDeviceSyncSchedule(int val)
{
    uspSyncTimeSched.setValue(QVariant(val));
    MContainer *con = uspSyncTimeSched.getModbusContainer();
    con->slave = slave;
    return (service->write( con ));

}

bool USODevice::setDeviceFlfsId(int val)
{
    uspFlFsTypeToRd.setValue( QVariant(val) );
    MContainer *con = uspFlFsTypeToRd.getModbusContainer();
    con->slave = slave;
    return (service->writew( con ));
}

bool USODevice::setDeviceFlfsPgRd(int val)
{
    uspFlFsRead.setValue( QVariant(val) );
    MContainer *con = uspFlFsRead.getModbusContainer();
    con->slave = slave;
    return (service->writew( con ));
}

bool USODevice::readFlfsPageTail(int tail, QByteArray& vals )
{
    if( tail >=USOD_PACKETS_PER_PAGE ){
        return false;
    }

    MContainer *con = uspFlFsPage[tail]->getModbusContainer();
    con->slave = slave;

    if( service->readw( con ) != true){
        return false;
    }

    uspFlFsPage[tail]->getValue( vals );

    return true;

}


bool USODevice::setCommand(int cmd)
{
    QLOG_DEBUG() << "Set command" << cmd;
    uspCommand.setAddress( cmd );
    uspCommand.setValue( QVariant(0x0001) );
    MContainer *con = uspCommand.getModbusContainer();
    con->slave = slave;
    return ( service->writew( con ));
}

bool USODevice::waitCommand(int cmd, int repeat)
{
    QLOG_DEBUG() << "Wait command" << cmd;
    QVariant result = QVariant(QVariant::Int);
    uspCommand.setAddress( cmd );
    MContainer *con = uspCommand.getModbusContainer();
    con->slave = slave;

    do{
        QLOG_DEBUG() << "Read now";
        if( service->readw( con ) != true){
            return false;
        }
        uspCommand.getValue(  result );

    }while( result.value<int>() && repeat-- );

    if( repeat > 0)
        return true;
    else
        return false;
}


void USODevice::connectSignals()
{
    connect( service, SIGNAL(resultRd(int,int)), this, SLOT(updateParameterRd_SLOT(int,int)), Qt::QueuedConnection);
    connect( service, SIGNAL(resultWr(int,int)), this, SLOT(updateParameterWr_SLOT(int,int)), Qt::QueuedConnection);
}


USODevice *USODevice::findFirstDevice(QObject *parent, const int startDeviceAddress, ModbusMaster *m)
{
    int address = startDeviceAddress;

    QLOG_DEBUG()<<"Find device at" << address;

    USODevice * uso = new USODevice(parent, address, m);

    QLOG_DEBUG()<<"Create device";

    uso->setSlaveAddress( address );
    QLOG_DEBUG()<<"Set slave address";
    MContainer* req = uso->uspTime.getModbusContainer();
    req->slave = address;

    if( uso->service->readw( req ) == false ){
        QLOG_DEBUG() << "Device not found at" << address << " result " << req->result;
        delete uso;
        return 0;
    }else if( req->result <= 0){
        QLOG_DEBUG() << "Device not found at" << address << " result " << req->result;
        delete uso;
        return 0;
    }else{
        QLOG_DEBUG() << "Find OK";
        QString str;
        uso->uspTime.getValue(str);
        uso->slave = address;
        QLOG_DEBUG() << str;
        return uso;
    }
}


void USODevice::updateParameterRd_SLOT(int aslave, int aaddress)
{
    if( aslave == this->slave){        
        QLOG_DEBUG() << "Read parameter slot known slave ";        

        //if( pmap.contains(aaddress)){
            QLOG_DEBUG() << "Read parameter slot, known address " << QString("0x%1").arg(aaddress*2,0,16);
            emit updateParameterRd_SIGN( aaddress );

            if( aaddress == uspTime.getAddress() ){                
                emit updDeviceTime();
            }else if( aaddress == uspFlFsInfo->getAddress()  ){
                    if(uspFlFsInfo  && uspFlFsInfo->isValid() ){
                    emit updDeviceFlash();
                }
            }else if( aaddress == uspSensorData.getAddress() ){
                QLOG_DEBUG() << "Sensor data emit";
                emit updSensorData();
            }else if( aaddress == uspSensorFlags.getAddress() ){
                QLOG_DEBUG() << "Sensor updte flags emit";
                emit updSensorUpdFlags();
            }
        //}
    }
}

void USODevice::updateParameterWr_SLOT(int aslave, int aaddress)
{
    if( aslave == this->slave){
        if( /*pmap.contains(aaddress)*/1 ){
            emit updateParameterWr_SIGN( aaddress );
        }
    }

}


/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file usodevice.cpp
 */
