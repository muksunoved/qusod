/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	pagewidget.cpp
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/23/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */



#include <ui_form.h>
#include <QSerialPortInfo>
#include <QsLog.h>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include "flashrddialog.h"

#include "pagewidget.h"
#include "setidpasswdialog.h"

PageWidget::PageWidget(QWidget *parent , USODevice* dev ) :
    QWidget(parent),
    usodev(dev),
    ui( new Ui::PageForm )
{
    ui->setupUi(this);
    enable = false;
    startTimer(1000);
    sensCount = 0;

    rawData = new RawDataModel(this);
    rawData->setMaxNoOfLines( 100 );

    ui->listView->setModel(rawData->model);
    rawData->enableAddLines( true );

    connect( usodev, SIGNAL(updDeviceTime()), this, SLOT(updateDevTime_SLOT()));
    connect( usodev, SIGNAL(updDeviceFlash()), this, SLOT(updateDevFlash_SLOT()));
    connect( usodev, SIGNAL(updSensorData()), this, SLOT(updateDevSensor_SLOT()));
    connect( usodev, SIGNAL(updSensorUpdFlags()), this, SLOT(updateDevSensorFlags_SLOT()));

    dev->refreshStaticParameters();

    QVariant val;
    dev->getDeviceSerialRom( val );
    ui->leDevID->setText( QString("%1").arg(val.value<MParameters::SSerialROM>().serial) );

    dev->getDeviceMBusSettings( val );
    ui->leDevAddress->setText( QString("%1").arg( val.value<MParameters::MBusSettings>().address ));


    int tmp;
    dev->getSyncSchedule( tmp );
    ui->leSyncSchedule->setText(QString("%1").arg(tmp));

    QList<qint32> serialBauds = QSerialPortInfo::standardBaudRates();

    foreach (qint32 baud, serialBauds) {
        ui->cbxBaud->addItem(QString("%1").arg(baud));
        if( baud == val.value<MParameters::MBusSettings>().baud){
            ui->cbxBaud->setCurrentIndex( ui->cbxBaud->count()-1 );
        }
    }
}

PageWidget::~PageWidget()
{
    if( usodev != NULL){
        //delete usodev;
    }
}

void PageWidget::setEnabled()
{
    QWidget::setEnabled( true );
    enable = true;

}

void PageWidget::setDisabled()
{
    QWidget::setDisabled(true);
    enable = false;
}

void PageWidget::updateDevTime_SLOT()
{
    if( usodev ){
        QVariant curDevTime(QVariant::String);
        usodev->getDeviceTime( curDevTime );
        ui->leDevTime->setText( curDevTime.toString() );
    }
}

void PageWidget::updateDevFlash_SLOT()
{
    if( usodev ){

        QVariant tmp;
        usodev->getDeviceFlashInfo( tmp );
        int records;
        int recordWr = records = (tmp.value<MParameters::FlashInfo>().pgWr*1024 + tmp.value<MParameters::FlashInfo>().pgWrOffs-32)/32;;

        if( tmp.value<MParameters::FlashInfo>().loops == 0 ){
            records = recordWr;
        }else{
            records = tmp.value<MParameters::FlashInfo>().totalBlocks*1024/32;
        }

        ui->leAvailRecords->setText( QString("%1").arg(records) );
        ui->leWrRecNum->setText( QString("%1").arg(recordWr) );

    }

}

void PageWidget::updateDevSensor_SLOT()
{
    if( usodev){
        QLOG_DEBUG() << "Update sensor data";
        QVariant tmp;
        usodev->getSensorData( tmp );
        MParameters::SensorRegs data = tmp.value<MParameters::SensorRegs>();
        QLOG_DEBUG() << "Sensor counter" << data.counter;
        //if( sensCount != data.counter){

            /*
             * rawData->addLine(QString("%1:%2:%3").arg((int)data.address).arg((int)data.counter).arg((uint)data.time) +
                             " [ " + QByteArray(data.vals, 11).toHex() + " ] ");
                             */
            QDateTime tme = QDateTime::fromTime_t( data.time + 946684800 ).toUTC();


            rawData->addLine(QString("%1:%2 ").arg((int)data.address).arg((int)data.counter) + tme.toString("dd-MM-yyyy hh:mm:ss") +
                             " [ " + QString("%1:%2:%3:%4").
                                        arg(data.uivals[0],8,16,QChar('0')).
                                        arg(data.uivals[1],8,16,QChar('0')).
                                        arg(data.uivals[2],8,16,QChar('0')).
                                        arg(data.uivals[3],8,16,QChar('0')) + " ] ");
            sensCount = data.counter;
        //}

    }

}

void PageWidget::updateDevSensorFlags_SLOT()
{
    if( usodev){
        QLOG_DEBUG() << "Update sensor flags";
        QVariant tmp;

        usodev->getSensorFlags(tmp);
        MParameters::SensorUpdFlags data = tmp.value<MParameters::SensorUpdFlags>();
        QLOG_DEBUG() << "Sensor flags " << data.flags[0] << " " << data.flags[1] << " " << data.flags[2] << " " << data.flags[3];

        for( unsigned int i =0; i<sizeof(data)*8; i++){
            if( (((uint)0x01 << (i%32)) & data.flags[i/32]) ){
                int addr = int(USODevice::sensRegs_0) + (int)52/2*i;
                ui->lblCurSensFlag->setText( QString("Current sens: %1").arg(i) );
                usodev->refreshSensorData( addr );
            }
        }
        usodev->setCommand( (int)USODevice::cmdFixUpdaterData );

    }

}

void PageWidget::timerEvent(QTimerEvent *)
{
    if( usodev && enable ){
        QLOG_DEBUG() << "Start Get periodical parameters -------";
        usodev->refreshDeviceTime();
        usodev->refreshDeviceFlashOffset();
        //usodev->refreshSensorData();
        usodev->refreshSensorFlags();
        QLOG_DEBUG() << "End Get periodical parameters -------";
    }

}

void PageWidget::on_pbUpdID_released()
{
    SetIDPasswDialog *dialog = new SetIDPasswDialog(this);

    dialog->exec();

    MParameters::SSerialROM tmp;
    tmp.password = dialog->getPassString().toInt();

    tmp.serial = ui->leDevID->text().toInt();

    tmp.enableCount  =0;
    usodev->setDeviceSerial( &tmp );

    usodev->setCommand( (int)USODevice::cmdSetNvRom );
    if(usodev->waitCommand( (int)USODevice::cmdSetNvRom) != true ){
        QLOG_ERROR() << "Set NV ROM Fail!";
    }
    delete dialog;
}

void PageWidget::on_pbSetTime_released()
{

    QDateTime tme = QDateTime::currentDateTime();
    MParameters::TimeStruct tmp;
    tmp.bDate = tme.date().day();
    tmp.bHour = tme.time().hour();
    tmp.bMin = tme.time().minute();
    tmp.bMonth = tme.date().month();
    tmp.bSec = tme.time().second();
    tmp.bWeekDay = tme.date().dayOfWeek();
    tmp.bYear = tme.date().year()%100;

    usodev->setDeviceTime( &tmp );
    usodev->setCommand((int)USODevice::cmdSetTime);

}

void PageWidget::on_pbUpdSettings_released()
{
    QVariant val;
    if( ui->cbxBaud->currentText().isEmpty() || ui->leDevAddress->text().isEmpty()){
        QLOG_DEBUG() << "Empty";
    }
    usodev->getDeviceMBusSettings( val );
    MParameters::MBusSettings mb = val.value<MParameters::MBusSettings>();
    mb.baud = ui->cbxBaud->currentText().toInt();
    mb.address = ui->leDevAddress->text().toInt();
    QLOG_DEBUG() << "Set address" << mb.address;
    QLOG_DEBUG() << "Set baud" << mb.baud;

    usodev->setDeviceMBusSettings(&mb);

    int valSync(ui->leSyncSchedule->text().toInt());

    QLOG_DEBUG() << "Set Schedule" << valSync;

    usodev->setDeviceSyncSchedule( valSync );

    usodev->setCommand((int)USODevice::cmdSetRom);

}

void PageWidget::on_pbSendSync_released()
{
    usodev->setCommand((int)USODevice::cmdSendSyncro );
}


void PageWidget::on_pbGetRecords_released()
{

    QByteArray raw;
    emit enablePause( true );
    enable = false; // set pause

    FlashRdDialog *dlg = new FlashRdDialog( this, usodev, &raw );

    dlg->exec();

    delete dlg;

    emit enablePause( false );

    QDateTime tme = QDateTime::currentDateTime();
    QDateTime sensTme;

    if( raw.size() < 1024){
        QMessageBox::critical(this, tr("Error"), tr("Insufficient data"));
        return;
    }


#if defined(Q_OS_WIN32)
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save file"), QApplication::applicationDirPath()+"\\reports\\"+ QString("dev%1-").arg(usodev->getSlaveAddress())+
                                                    tme.toString("dd-MM-yyyy-hh-mm-ss")+".txt" , tr("Files (*.txt)"));
#else
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save file"), QApplication::applicationDirPath()+"/reports/"+ QString("dev%1-").arg(usodev->getSlaveAddress())+
                                                    tme.toString("dd-MM-yyyy-hh-mm-ss")+".txt" , tr("Files (*.txt)"));
#endif

    if( !fileName.isEmpty() ){

        QFile f( fileName );
        if( f.open( QIODevice::WriteOnly | QIODevice::Text ) ){

            QTextStream text( &f );

            text << "USODR device at modbus [" << usodev->getSlaveAddress() <<"]" << " Flash report" << endl;
            text << "absolute seconds.ms\tsensor num\tcounter\tvalue0\tvalue1\tvalue2\tvalue3" << endl;



            SFLFSRec rec;
            char chData[sizeof(SFLFSRec)];

            for( int i=raw.size(); i>0; i-=1024){

                QByteArray rawTail;

                if(raw.size() >= 1024){
                    rawTail = raw.right(1024);
                    raw.resize(raw.size()-1024);
                }else{
                    break;
                }

                //for( int l=rawTail.size()-sizeof(SFLFSRec); l>0; l-=sizeof(SFLFSRec)  ){
                for( int l=0; l<rawTail.size(); l+=sizeof(SFLFSRec)  ){
                    for(int k=0; k<sizeof(SFLFSRec); k++){
                        chData[k] = rawTail[l+k];
                    }
                    memcpy((void*)&rec, (void*)chData, sizeof(SFLFSRec));
                    if( rec.hdr != REC_HDR_MTU ){

                        text << "# unknown" << endl;

                    }else{

                        quint32 val32[4];

                        for(int k=0; k<4; k++){
                            val32[k] = 0;
                            val32[k] |= rec.con.fLongVal.vals[k*3+0]; val32[k] <<=8;
                            val32[k] |= rec.con.fLongVal.vals[k*3+1];
                            if(k<3){
                                val32[k] <<=8;
                                val32[k] |= rec.con.fLongVal.vals[k*3+2];
                            }
                        }

                        sensTme = QDateTime::fromTime_t( rec.con.fLongVal.seconds + 946684800 ).toUTC();

                        text << QString("%1.%2 %3 %4 %5 %6 %7 %8 ").arg(rec.con.fLongVal.seconds,10,10).arg(rec.con.fLongVal.msec,-4,10).
                                arg(rec.con.fLongVal.sensAddr,5,10,QChar('0')).arg(rec.con.fLongVal.counter,10,10).
                                arg(val32[0],8,16,QChar('0')).
                                arg(val32[1],8,16,QChar('0')).
                                arg(val32[2],8,16,QChar('0')).
                                arg(val32[3],8,16,QChar('0')) << sensTme.toString("dd-MM-yyyy hh:mm:ss") << endl;
                    }
                }
            }

            f.flush();
            f.close();

        }else{
           QMessageBox::critical(this, tr("Error"), tr("Save file error"));
           return;
        }

    }
}


/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file pagewidget.cpp
 */
