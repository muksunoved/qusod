/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	pagewidget.h
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/23/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */


#ifndef PAGEWIDGET_H
#define PAGEWIDGET_H

#include <QWidget>
#include "usodevice.h"
#include <QTimer>
#include "rawdatamodel.h"

namespace Ui {
    class PageForm;
}

class PageWidget : public QWidget
{
    Q_OBJECT

public:    
    PageWidget(QWidget *parent = 0, USODevice* dev = 0);
    ~PageWidget();
    int getUsoSlaveAddress() { if( usodev ){ return usodev->getSlaveAddress();} else {return 0 ;} }
    void setEnabled();
    void setDisabled();

signals:
    void enablePause( bool set);

public slots:
    void updateDevTime_SLOT();
    void updateDevFlash_SLOT();
    void updateDevSensor_SLOT();
    void updateDevSensorFlags_SLOT();

private:
    Ui::PageForm    *ui;
    QTimer          timer;
    USODevice       *usodev;
    unsigned int    sensCount;
    RawDataModel    *rawData;
    bool enable;

protected:
    virtual void  timerEvent(QTimerEvent * );

private slots:
    void on_pbUpdID_released();
    void on_pbSetTime_released();
    void on_pbUpdSettings_released();
    void on_pbSendSync_released();
    void on_pbGetRecords_released();
};

#endif // PAGEWIDGET_H

/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file pagewidget.h
 */
