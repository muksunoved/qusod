/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	usodwidget.h
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/14/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */




#ifndef USODWIDGET_H
#define USODWIDGET_H

#include <QWidget>
#include "modbusmaster.h"
#include "pagewidget.h"

namespace Ui {
class UsodWidget;
}

class UsodWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UsodWidget(QWidget *parent = 0);
    ~UsodWidget();

private slots:
    void on_pbConnect_released();    

    void on_pbFind_released();

    void on_tabDevices_tabCloseRequested(int index);

    void enablePause(bool set);

private:
    Ui::UsodWidget *ui;
    ModbusMaster m;

    QList<PageWidget*> devPages;
};

#endif // USODWIDGET_H


/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file usodwidget.h
 */
