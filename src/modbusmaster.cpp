/*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Alfred Latypov:
*          e-mail: <alfred@grant-ufa.ru>
*             ICQ: 2052354
*/

/**
 *
 *  @file	modbusmaster.cpp
 *  @author	alfredk
 *  @version	v0.0.0
 *  @date	10/15/2014
 *  @brief	This file description
 *
 *  @attention
 *      No attentions
 *
 *
 *  <h3><center>&copy; Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)</center></h3>
 *
 */


#include "QsLog.h"

#if defined(Q_OS_LINUX)
    #include <errno.h>
#endif

#include <modbus-rtu.h>
#include <modbus.h>
#include <QWaitCondition>
#include <QSemaphore>


#include "modbusmaster.h"



#define SERTHREAD_DEBUG

#if defined( SERTHREAD_DEBUG )
    #define MOD_DEBUG( X )  {       \
          qDebug() << X << endl;    \
    }
#else
    #define MOD_DEBUG( X )  {}
#endif


ModbusMaster* m_instance;



ModbusMaster::ModbusMaster(QObject *parent) :
    QThread(parent), startWr(false), startRd(false), devStart(false), devStopped(true), devFail(false)
{        
    m_instance = this;

    lockQRd.release();
    lockQWr.release();

    rdErrors = wrErrors = 0;
}

ModbusMaster::~ModbusMaster()
{
    QLOG_DEBUG()<< "delete modbusmaster";
    portdisconnect();
}

bool ModbusMaster::portconnect(const QString &aportName, const quint32 abaud)
{
    devFail     = false;
    portName    = aportName;
    baud        = abaud;
    int n = 40;

    if( devStart ){
        portdisconnect();
    }

    QLOG_DEBUG() << "Unlock sema";

    lockQRd.release();
    lockQWr.release();

    devStart = true;

    QLOG_INFO() << "Start";
    start();
    QLOG_INFO() << "Started";

    while( !isRunning() && --n ){
        msleep(200);
    }
    QLOG_INFO() << "RUN";
    n = 50;
    while( devStopped && --n && !devFail){
        msleep(100);
    }
    QLOG_INFO() << "RUN Lock";

    return devStart;
}

void ModbusMaster::portdisconnect()
{
    devStart = false;
    startWr = startRd = false;
    QLOG_DEBUG() << "Disconect";
    lockSleep.release();

    msleep(1000);

}

bool ModbusMaster::write( MContainer* w )
{
    if( w == NULL ){
        return false;
    }
    if( !isRunning()){
        return false;
    }

    w->waiter = true;
    QLOG_DEBUG() << "w_waiter unlock";
    lockQWr.acquire();
    QLOG_DEBUG() << "wdata locked";
    wrData.enqueue(w);
    startWr = true;
    lockQWr.release();
    QLOG_DEBUG() << "wdata unlocked";

    lockSleep.release();
    QLOG_DEBUG() << "thread wakeup";

    return true;

}

bool ModbusMaster::writew(MContainer *w)
{
    QLOG_DEBUG() << "writedw";
    if( !write(w)){
        return false;
    }else{
        while( w->waiter ){
            msleep(10);
        }
    }
    return true;
}

bool ModbusMaster::read( MContainer* r )
{
    if( r == NULL ){
        return false;
    }
    if( !isRunning()){
        return false;
    }
    QLOG_DEBUG() << "read";

    r->waiter = true;
    QLOG_DEBUG() << "waiter unlock";
    lockQRd.acquire();
    QLOG_DEBUG() << "rdata locked";
    rdData.enqueue(r);
    startRd = true;
    lockQRd.release();
    QLOG_DEBUG() << "rdata unlocked";

    lockSleep.release();
    QLOG_DEBUG() << "thread wakeup";

    return true;

}

bool ModbusMaster::readw( MContainer* r )
{
    QLOG_DEBUG() << "readw";
    if( !read(r)){
        return false;
    }else{
        while(r->waiter){
            msleep(10);
        }
    }
    return true;
}

void ModbusMaster::busMonitorRequestData(uint8_t *data, uint8_t dataLen)
{
    // callback from libusb
    QString line;

    for(int i = 0; i < dataLen; ++i ) {
        line += QString().sprintf( "%.2x  ", data[i] );
    }

    QLOG_INFO() << "Tx Data : " << line;
}

void ModbusMaster::busMonitorResponseData(uint8_t *data, uint8_t dataLen)
{
    // callback from libusb
    QString line;

    for(int i = 0; i < dataLen; ++i ) {
        line += QString().sprintf( "%.2x  ", data[i] );
    }

    QLOG_INFO() << "Rx Data : " << line;
}

void ModbusMaster::run()
{
    int result;
    devStart = false;
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    // open port
    modbus_t* ctx = modbus_new_rtu(portName.toLatin1().data(), baud, 'N', 8, 1, 0);
    if( ctx == 0){
        QLOG_ERROR() << "Fail get ctx " << modbus_strerror(errno);
        devFail = true;
        rdData.clear();
        wrData.clear();
        devStopped = true;
        return;

    }else{
        QLOG_DEBUG() << "Get ctx OK ";
    }

    if( modbus_connect(ctx) == -1 ){
        QLOG_ERROR() << "Fail connect " << modbus_strerror(errno);
        devFail = true;
        rdData.clear();
        wrData.clear();
        devStopped = true;
        return;
    }else{
        QLOG_DEBUG() << "Connect OK";
    }
    //modbus_set_debug( ctx, 1 );
    //modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS232);
    //modbus_rtu_set_rts( ctx, MODBUS_RTU_RTS_NONE);

    modbus_set_response_timeout(ctx, &timeout );        

    devStart = true;
    devFail = false;
    lockSleep.release();

    QLOG_INFO() << "Goto Run ";

    forever {

        //lockSleep.lock();   // sleep

        if( devStart ) {
            msleep(10);
            devStopped = false;
            if( startWr || wrData.size() ){
                MContainer* wr = 0;
                lockQWr.acquire();
                if( wrData.size()){
                    wr = wrData.dequeue();
                }
                lockQWr.release();
                if( wr == 0 ){
                    QLOG_DEBUG() << "wr container is 0!";
                    continue;
                }
                modbus_set_slave( ctx, wr->slave );


                if( (result = modbus_write_registers( ctx, wr->address, wr->len, wr->data)) < 0 ){
                    QLOG_DEBUG() << "Fail write " << result <<":" << modbus_strerror(errno);
                    wr->result = result;
                    wr->waiter = false;
                    wrErrors++;
                    msleep(10);
                    //emit resultWr( wr->slave, wr->address );
                    emit resulWrError( wrErrors );
                }else{
                    wr->result = result;
                    wr->waiter = false;
                    msleep(10);
                    emit resultWr( wr->slave, wr->address );
                }
                startWr = 0;
            }
            if( startRd || rdData.size() ){
                MContainer* rd = 0;
                lockQRd.acquire();
                if( rdData.size()){
                    rd = rdData.dequeue();
                }
                lockQRd.release();

                if( rd == 0 ){
                    QLOG_DEBUG() << "rd container is 0!";
                    continue;
                }
                QLOG_DEBUG() << "Set slave" << rd->slave;
                modbus_set_slave( ctx, rd->slave );


                if( (result = modbus_read_registers( ctx, rd->address, rd->len, rd->data)) < 0 ){
                    QLOG_DEBUG() << "Fail read " << result <<":" << modbus_strerror(errno);
                    rd->result = result;
                    msleep(10);
                    rd->waiter = false;
                    //emit resultRd( rd->slave, rd->address );
                    rdErrors++;
                    emit resulRdError( rdErrors );
                }else{
                    rd->result = result;
                    rd->waiter = false;
                    msleep(10);
                    emit resultRd( rd->slave, rd->address );
                }
                // ulock data only if read
                startRd = 0;
            }
        }else{
            devFail = false;
            modbus_close(ctx);
            MContainer* con;
            foreach (con, rdData) {
                con->result = -1;
                con->waiter = false;
            }
            foreach (con, wrData) {
                con->result = -1;
                con->waiter = false;
            }
            rdData.clear();
            wrData.clear();
            devStopped = true;
            return;
        }
    }
}


extern "C" {

void busMonitorRawResponseData(uint8_t * data, uint8_t dataLen)
{
        m_instance->busMonitorResponseData(data, dataLen);
}

void busMonitorRawRequestData(uint8_t * data, uint8_t dataLen)
{
        m_instance->busMonitorRequestData(data, dataLen);
}

}

/*
 * Copyright 2014 GK Grant Ufa (www.grant-ufa.ru)
 * End of file modbusmaster.cpp
 */
